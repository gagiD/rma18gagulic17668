package unsa.rma.rma18gagulic17668.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import unsa.rma.rma18gagulic17668.entities.Knjiga;


public class KnjigaRep {
    private static final String TABLE        = "Knjige";
    private static final String KEY_ID       = "Id";
    private static final String KEY_KAT_ID   = "KatId";
    private static final String KEY_NAZIV    = "Naziv";
    private static final String KEY_AUTOR    = "Autor";
    private static final String KEY_SLIKA    = "Slika";
    private static final String KEY_SELECTED = "Selected";


    public static String getCreateTableQuery() {
        return "CREATE TABLE " + TABLE + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_KAT_ID + " INTEGER, "
                + KEY_NAZIV + " TEXT, "
                + KEY_AUTOR + " TEXT, "
                + KEY_SLIKA + " TEXT, "
                + KEY_SELECTED + " INTEGER)";
    }

    public static void insert(Knjiga knjiga) {
        SQLiteDatabase db     = DatabaseManager.getInstance().openDatabase();
        ContentValues  values = new ContentValues();

        values.put(KEY_KAT_ID  , knjiga.KategorijaId);
        values.put(KEY_NAZIV   , knjiga.Naziv);
        values.put(KEY_AUTOR   , knjiga.Autor);
        values.put(KEY_SLIKA   , knjiga.Slika);
        values.put(KEY_SELECTED, knjiga.Selected);

        db.insert(TABLE, null, values);

        DatabaseManager.getInstance().closeDatabase();
    }

    public static void update(Knjiga knjiga) {
        SQLiteDatabase db    = DatabaseManager.getInstance().openDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_KAT_ID  , knjiga.KategorijaId);
        values.put(KEY_NAZIV   , knjiga.Naziv);
        values.put(KEY_AUTOR   , knjiga.Autor);
        values.put(KEY_SLIKA   , knjiga.Slika);
        values.put(KEY_SELECTED, knjiga.Selected);

        db.update(TABLE, values, KEY_ID + " = ?", new String[]{String.valueOf(knjiga.Id)});

        DatabaseManager.getInstance().closeDatabase();
    }

    public static void insertOrUpdate(Knjiga knjiga) {
        if (getById(knjiga.Id) == null)
            insert(knjiga);
        else
            update(knjiga);
    }

    public static void delete(Knjiga knjiga) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

        db.delete(TABLE, KEY_ID + "  = ?", new String[]{String.valueOf(knjiga.Id)});

        DatabaseManager.getInstance().closeDatabase();
    }

    public static void delete(long knjigaId) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

        db.delete(TABLE, KEY_ID + " = ?", new String[]{String.valueOf(knjigaId)});

        DatabaseManager.getInstance().closeDatabase();
    }

    public static void deleteAll() {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

        db.delete(TABLE, KEY_ID + " >= ?", new String[]{String.valueOf(0)});

        DatabaseManager.getInstance().closeDatabase();
    }

    public static String getPosjetaSelectQuery(String WhereClause) {
        return "SELECT "
               + KEY_ID + ", "
               + KEY_KAT_ID + ", "
               + KEY_NAZIV + ", "
               + KEY_AUTOR + ", "
               + KEY_SLIKA + ", "
               + KEY_SELECTED
               + " FROM " + TABLE
               + WhereClause;
    }

    public static String getPosjetaSelectQuery() {
        return getPosjetaSelectQuery("");
    }

    public static Knjiga getById(long knjigaId) {
        Knjiga knjiga    = new Knjiga();
        SQLiteDatabase db  = DatabaseManager.getInstance().openDatabase();
        String selectQuery = getPosjetaSelectQuery(" WHERE " + KEY_ID + " =" + String.valueOf(knjigaId));


        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                knjiga.Id           = cursor.getInt(cursor.getColumnIndex(KEY_ID));
                knjiga.KategorijaId = cursor.getInt(cursor.getColumnIndex(KEY_KAT_ID));
                knjiga.Naziv        = cursor.getString(cursor.getColumnIndex(KEY_NAZIV));
                knjiga.Autor        = cursor.getString(cursor.getColumnIndex(KEY_AUTOR));
                knjiga.Slika        = cursor.getString(cursor.getColumnIndex(KEY_SLIKA));
                knjiga.Selected     = cursor.getInt(cursor.getColumnIndex(KEY_SELECTED)) != 0;
            } else {
                knjiga = null;
            }

            cursor.close();
            DatabaseManager.getInstance().closeDatabase();
        } catch (Exception ex) {
            ex.printStackTrace();
            knjiga = null;
        }

        return knjiga;
    }

    public static ArrayList<Knjiga> getByKatId(long katId) {
        ArrayList<Knjiga> knjige = new ArrayList<>();

        SQLiteDatabase db  = DatabaseManager.getInstance().openDatabase();
        String selectQuery = getPosjetaSelectQuery(" WHERE " + KEY_KAT_ID + " =" + String.valueOf(katId));

        try {
            Cursor cursor = db.rawQuery(selectQuery, null);


            if (cursor.moveToFirst()) {
                do {
                    Knjiga knjiga = new Knjiga();

                    knjiga.Id           = cursor.getInt(cursor.getColumnIndex(KEY_ID));
                    knjiga.KategorijaId = cursor.getInt(cursor.getColumnIndex(KEY_KAT_ID));
                    knjiga.Naziv        = cursor.getString(cursor.getColumnIndex(KEY_NAZIV));
                    knjiga.Autor        = cursor.getString(cursor.getColumnIndex(KEY_AUTOR));
                    knjiga.Slika        = cursor.getString(cursor.getColumnIndex(KEY_SLIKA));
                    knjiga.Selected     = cursor.getInt(cursor.getColumnIndex(KEY_SELECTED)) != 0;

                    knjige.add(knjiga);
                } while (cursor.moveToNext());
            } else {
                knjige = null;
            }

            cursor.close();
            DatabaseManager.getInstance().closeDatabase();
        } catch (Exception ex) {
            ex.printStackTrace();
            knjige = null;
        }

        return knjige;
    }
}
