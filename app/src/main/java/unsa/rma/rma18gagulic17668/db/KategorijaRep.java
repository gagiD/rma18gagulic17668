package unsa.rma.rma18gagulic17668.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import unsa.rma.rma18gagulic17668.entities.Kategorija;


public class KategorijaRep {
    private static final String TABLE     = "Kategorije";
    private static final String KEY_ID    = "Id";
    private static final String KEY_NAZIV = "Naziv";


    public static String getCreateTableQuery() {
        return "CREATE TABLE " + TABLE + "("
               + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
               + KEY_NAZIV + " TEXT)";
    }

    public static void insert(Kategorija kategorija) {
        SQLiteDatabase db     = DatabaseManager.getInstance().openDatabase();
        ContentValues  values = new ContentValues();

        values.put(KEY_NAZIV, kategorija.Naziv);

        db.insert(TABLE, null, values);

        DatabaseManager.getInstance().closeDatabase();
    }

    public static void update(Kategorija kategorija) {
        SQLiteDatabase db    = DatabaseManager.getInstance().openDatabase();
        ContentValues values = new ContentValues();


        values.put(KEY_NAZIV, kategorija.Naziv);

        db.update(TABLE, values, KEY_ID + " = ?", new String[]{String.valueOf(kategorija.Id)});

        DatabaseManager.getInstance().closeDatabase();
    }

    public static void insertOrUpdate(Kategorija kategorija) {
        if (getById(kategorija.Id) == null)
            insert(kategorija);
        else
            update(kategorija);
    }

    public static void delete(Kategorija kategorija) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

        db.delete(TABLE, KEY_ID + "  = ?", new String[]{String.valueOf(kategorija.Id)});

        DatabaseManager.getInstance().closeDatabase();
    }

    public static void delete(long kategorijaId) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

        db.delete(TABLE, KEY_ID + " = ?", new String[]{String.valueOf(kategorijaId)});

        DatabaseManager.getInstance().closeDatabase();
    }

    public static void deleteAll() {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

        db.delete(TABLE, KEY_ID + " >= ?", new String[]{String.valueOf(0)});

        DatabaseManager.getInstance().closeDatabase();
    }

    public static String getPosjetaSelectQuery(String WhereClause) {
        return "SELECT "
               + KEY_ID + ", "
               + KEY_NAZIV
               + " FROM " + TABLE
               + WhereClause;
    }

    public static String getPosjetaSelectQuery() {
        return getPosjetaSelectQuery("");
    }

    public static ArrayList<Kategorija> getAll() {
        ArrayList<Kategorija> kategorije = new ArrayList<>();
        SQLiteDatabase db  = DatabaseManager.getInstance().openDatabase();
        String selectQuery = getPosjetaSelectQuery();

        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Kategorija kategorija = new Kategorija();

                    kategorija.Id           = cursor.getInt(cursor.getColumnIndex(KEY_ID));
                    kategorija.Naziv        = cursor.getString(cursor.getColumnIndex(KEY_NAZIV));

                    kategorije.add(kategorija);
                } while (cursor.moveToNext());
            } else {
                kategorije = null;
            }

            cursor.close();
            DatabaseManager.getInstance().closeDatabase();
        } catch (Exception ex) {
            ex.printStackTrace();
            kategorije = null;
        }

        return kategorije;
    }

    public static Kategorija getById(long kategorijaId) {
        Kategorija kategorija = new Kategorija();
        SQLiteDatabase db  = DatabaseManager.getInstance().openDatabase();
        String selectQuery = getPosjetaSelectQuery(" WHERE " + KEY_ID + " =" + String.valueOf(kategorijaId));


        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                kategorija.Id    = cursor.getInt(cursor.getColumnIndex(KEY_ID));
                kategorija.Naziv = cursor.getString(cursor.getColumnIndex(KEY_NAZIV));
            } else {
                kategorija = null;
            }

            cursor.close();
            DatabaseManager.getInstance().closeDatabase();
        } catch (Exception ex) {
            ex.printStackTrace();
            kategorija = null;
        }

        return kategorija;
    }
}
