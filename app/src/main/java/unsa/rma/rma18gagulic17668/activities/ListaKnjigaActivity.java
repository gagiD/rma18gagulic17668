package unsa.rma.rma18gagulic17668.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import unsa.rma.rma18gagulic17668.R;
import unsa.rma.rma18gagulic17668.adapters.KnjigaAdapter;


public class ListaKnjigaActivity extends AppCompatActivity {
    public static String KAT_ID_EXTRA = "KatIdExtra";

    private ListView      listaKnjiga;
    private KnjigaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_knjiga);

        Intent intent = getIntent();
        int katId = intent.getIntExtra(KAT_ID_EXTRA, -1);

        listaKnjiga = findViewById(R.id.listaKnjiga);

        adapter = new KnjigaAdapter(this, katId);
        listaKnjiga.setAdapter(adapter);
    }
}
