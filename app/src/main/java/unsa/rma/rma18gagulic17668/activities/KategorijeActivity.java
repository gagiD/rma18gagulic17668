package unsa.rma.rma18gagulic17668.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import unsa.rma.rma18gagulic17668.R;
import unsa.rma.rma18gagulic17668.adapters.KategorijaAdapter;
import unsa.rma.rma18gagulic17668.adapters.OnEmptyResult;
import unsa.rma.rma18gagulic17668.db.KategorijaRep;
import unsa.rma.rma18gagulic17668.entities.Kategorija;


public class KategorijeActivity extends AppCompatActivity {

    private EditText textPretraga;
    private Button   dPretraga;
    private Button   dDodajKat;
    private Button   dDodajBook;

    private ListView            listaKategorija;
    private KategorijaAdapter   kategorijaAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategorije);

        textPretraga = findViewById(R.id.tekstPretraga);
        dPretraga    = findViewById(R.id.dPretraga);
        dDodajKat    = findViewById(R.id.dDodajKategoriju);
        dDodajBook   = findViewById(R.id.dDodajKnjigu);

        listaKategorija = findViewById(R.id.listaKategorija);

        kategorijaAdapter = new KategorijaAdapter(this);
        kategorijaAdapter.setOnEmptyResult(new OnEmptyResult() {
            @Override
            public void onEmptyResult() {
                dDodajKat.setEnabled(true);
            }
        });

        listaKategorija.setAdapter(kategorijaAdapter);


        dDodajKat.setEnabled(false);

        dPretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dDodajKat.setEnabled(false);

                filterKat();
            }
        });

        dDodajKat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = textPretraga.getText().toString();
                if (!s.isEmpty()) {
                    Kategorija k = new Kategorija();
                    k.Naziv = s;

                    KategorijaRep.insert(k);

                    kategorijaAdapter.add(k);
                }

                dDodajKat.setEnabled(false);
            }
        });

        dDodajBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(KategorijeActivity.this, DodavanjeKnjigeActivity.class);
                startActivity(intent);
            }
        });
    }

    private void filterKat() {
        String s = textPretraga.getText().toString();

        kategorijaAdapter.filter(s);
    }
}
