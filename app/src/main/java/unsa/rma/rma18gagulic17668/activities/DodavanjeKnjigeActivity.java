package unsa.rma.rma18gagulic17668.activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import unsa.rma.rma18gagulic17668.R;
import unsa.rma.rma18gagulic17668.db.KategorijaRep;
import unsa.rma.rma18gagulic17668.db.KnjigaRep;
import unsa.rma.rma18gagulic17668.entities.Kategorija;
import unsa.rma.rma18gagulic17668.entities.Knjiga;


public class DodavanjeKnjigeActivity extends AppCompatActivity {
    public static final int PICK_IMAGE = 1;

    private EditText naslov;
    private EditText autor;
    private Button   slika;
    private Spinner  kategorija;

    private Button cancel;
    private Button save;

    private String curImage;
    private int    curKatId;

    private ArrayList<Integer> katIds;
    private ArrayList<String> katNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodavanje_knjige);

        naslov      = findViewById(R.id.book_naziv);
        autor       = findViewById(R.id.book_autor);
        slika       = findViewById(R.id.dNadiSliku);
        kategorija  = findViewById(R.id.book_kategorija);

        cancel = findViewById(R.id.book_cancel);
        save   = findViewById(R.id.book_save);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Knjiga knjiga = new Knjiga();
                knjiga.Naziv = naslov.getText().toString();
                knjiga.Autor = autor.getText().toString();
                knjiga.Slika = curImage;
                knjiga.KategorijaId = curKatId;
                knjiga.Selected = false;

                if (knjiga.Naziv.isEmpty())
                    naslov.setError("Required!");
                else if (knjiga.Autor.isEmpty())
                    autor.setError("Required!");
                else {
                    KnjigaRep.insert(knjiga);

                    finish();
                }
            }
        });

        slika.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Izaberite sliku"), PICK_IMAGE);
            }
        });

        katIds = new ArrayList<>();
        katNames = new ArrayList<>();

        ArrayList<Kategorija> kts = KategorijaRep.getAll();
        if (kts != null)
            for (Kategorija k : kts) {
                katIds.add(k.Id);
                katNames.add(k.Naziv);
            }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, katNames);

        kategorija.setAdapter(adapter);

        kategorija.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                curKatId = katIds.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            try {
                curImage = UUID.randomUUID().toString();

                FileOutputStream outputStream;
                outputStream = openFileOutput(curImage, Context.MODE_PRIVATE);
                getBitmapFromUri(data.getData()).compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor       fileDescriptor       = parcelFileDescriptor.getFileDescriptor();
        Bitmap               image                = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }
}
