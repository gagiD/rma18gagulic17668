package unsa.rma.rma18gagulic17668.entities;


public class Knjiga {
    public int     Id;
    public int     KategorijaId;
    public String  Naziv;
    public String  Autor;
    public String  Slika;
    public boolean Selected;
}
