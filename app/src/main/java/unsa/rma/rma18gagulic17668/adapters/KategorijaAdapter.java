package unsa.rma.rma18gagulic17668.adapters;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import unsa.rma.rma18gagulic17668.R;
import unsa.rma.rma18gagulic17668.activities.ListaKnjigaActivity;
import unsa.rma.rma18gagulic17668.db.KategorijaRep;
import unsa.rma.rma18gagulic17668.entities.Kategorija;


public class KategorijaAdapter extends BaseAdapter {
    private static ArrayList<Kategorija> allKats;
    private static ArrayList<Kategorija> curKats;

    private Context mContext;

    private OnEmptyResult mOnEmptyResult;

    public KategorijaAdapter(Context context) {
        mContext = context;

        allKats = KategorijaRep.getAll();
        curKats = KategorijaRep.getAll();

        if (allKats == null) {
            allKats = new ArrayList<>();
            curKats = new ArrayList<>();
        }
    }

    @Override
    public int getCount() {
        return curKats.size();
    }

    @Override
    public Kategorija getItem(int position) {
        return curKats.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(R.layout.kategorija_item, null);
        }

        final Kategorija kategorija = getItem(position);

        if (kategorija != null) {
            TextView naziv = v.findViewById(R.id.kategorija_naziv);

            naziv.setText(kategorija.Naziv);
        }

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (kategorija != null) {
                    Intent intent = new Intent(mContext, ListaKnjigaActivity.class);
                    intent.putExtra(ListaKnjigaActivity.KAT_ID_EXTRA, kategorija.Id);
                    mContext.startActivity(intent);
                }
            }
        });

        return v;
    }

    public void filter(String s) {
        curKats.clear();

        if (s.isEmpty())
            curKats.addAll(allKats);
        else
            for (Kategorija k : allKats)
                if (k.Naziv.contains(s))
                    curKats.add(k);

        notifyDataSetChanged();

        if (curKats.isEmpty())
            if (mOnEmptyResult != null)
                mOnEmptyResult.onEmptyResult();

    }

    public void setOnEmptyResult(OnEmptyResult callback) {
        mOnEmptyResult = callback;
    }

    public void add(Kategorija k) {
        allKats.add(k);
        curKats.add(k);

        notifyDataSetChanged();
    }
}
