package unsa.rma.rma18gagulic17668.adapters;


import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import unsa.rma.rma18gagulic17668.R;
import unsa.rma.rma18gagulic17668.db.KnjigaRep;
import unsa.rma.rma18gagulic17668.entities.Knjiga;


public class KnjigaAdapter extends BaseAdapter {
    private static ArrayList<Knjiga> knjige;

    private Context mContext;

    public KnjigaAdapter(Context context, int katId) {
        mContext = context;

        knjige = KnjigaRep.getByKatId(katId);
        if (knjige == null)
            knjige = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return knjige.size();
    }

    @Override
    public Knjiga getItem(int position) {
        return knjige.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View         v = convertView;
        final Knjiga knjiga;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(R.layout.knjiga_item, null);
        }

        knjiga = getItem(position);

        if (knjiga != null) {
            TextView naziv  = v.findViewById(R.id.book_item_naslov);
            TextView autor  = v.findViewById(R.id.book_item_autor);
            ImageView slika = v.findViewById(R.id.book_item_slika);

            naziv.setText(knjiga.Naziv);
            autor.setText(knjiga.Autor);

            try {
                slika.setImageBitmap(BitmapFactory.decodeStream(mContext.openFileInput(knjiga.Slika)));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (knjiga.Selected)
                v.setBackgroundColor(v.getResources().getColor(R.color.primary_light));
            else
                v.setBackgroundColor(v.getResources().getColor(R.color.white));
        }

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (knjiga != null) {
                    knjiga.Selected = !knjiga.Selected;

                    KnjigaRep.update(knjiga);

                    Knjiga listKnjiga = getItem(position);
                    listKnjiga.Selected = knjiga.Selected;

                    notifyDataSetChanged();
                }
            }
        });

        return v;
    }
}