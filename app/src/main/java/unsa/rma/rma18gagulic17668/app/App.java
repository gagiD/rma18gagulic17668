package unsa.rma.rma18gagulic17668.app;


import android.app.Application;
import android.content.Context;

import unsa.rma.rma18gagulic17668.db.DatabaseManager;
import unsa.rma.rma18gagulic17668.db.DbHandler;


public class App extends Application {
    private static Context   context;
    private static DbHandler dbHandler;

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();

        dbHandler = new DbHandler();

        DatabaseManager.initializeInstance(dbHandler);
    }

    public static Context getContext() {
        return context;
    }
}
